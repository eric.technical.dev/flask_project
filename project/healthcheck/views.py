# project/users/views.py
 
 
#################
#### imports ####
#################
 
from flask import render_template, Blueprint, current_app as app

 
 
################
#### config ####
################
 
healthcheck_blueprint = Blueprint('healthcheck', __name__, template_folder='templates')
 
 
################
#### routes ####
################
 
@healthcheck_blueprint.route('/health')
def health():
    return "Application running!!!"
    
# @TODO: return json format    
@healthcheck_blueprint.route('/infos')
def infos():
    return "MONGO_HOST={},MONGO_PORT={}".format(app.config["MONGO_HOST"],app.config["MONGO_PORT"])    