#################
#### imports ####
#################
 
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os
from flask_pymongo import PyMongo
import logging, logging.handlers
 
################
#### config ####
################

LEVEL = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR,
    "CRITICAL":logging.CRITICAL,
}
 
app = Flask(__name__, instance_relative_config=True)
app.logger.setLevel(logging.DEBUG)


cfg_settings = os.getenv('FLASK_CFG')
app.config.from_pyfile(cfg_settings)

# log settings
LogLevel = LEVEL[app.config["LOG_LEVEL"]]
file_handler = logging.handlers.RotatingFileHandler(app.config["LOG_FILENAME"], maxBytes=app.config["LOG_MAXBYTES"],  backupCount=app.config["LOG_BACKUPCOUNT"],  encoding=app.config["LOG_ENCODING"])
file_handler.setLevel(LogLevel)
file_handler_formater  = logging.Formatter(app.config["LOG_FORMAT"])
file_handler.setFormatter(file_handler_formater)
app.logger.addHandler(file_handler)


# postgresql settings
db = SQLAlchemy(app)

# mongodb settings
mongoDb = PyMongo(app)

 
####################
#### blueprints ####
####################
 
from project.users.views import users_blueprint
from project.recipes.views import recipes_blueprint
from project.healthcheck.views import healthcheck_blueprint
from project.messages.views import messages_blueprint

 
# register the blueprints
app.register_blueprint(users_blueprint)
app.register_blueprint(recipes_blueprint)
app.register_blueprint(healthcheck_blueprint)
app.register_blueprint(messages_blueprint)


############################
#### custom error pages ####
############################
 
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
 
 
@app.errorhandler(403)
def page_not_found(e):
    return render_template('403.html'), 403
 
 
@app.errorhandler(410)
def page_not_found(e):
    return render_template('410.html'), 410

