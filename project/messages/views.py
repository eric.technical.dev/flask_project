#################
#### imports ####
#################
 
from flask import render_template, request, redirect, url_for, Blueprint, jsonify
from project import mongoDb
import json
from bson import json_util
from bson.objectid import ObjectId
 
 
################
#### config ####
################
 
messages_blueprint = Blueprint('messages', __name__, template_folder='templates')
 

################
#### helpers ####
################

class JSONEncoder(json.JSONEncoder):
  ''' extend json-encoder class'''

  def default(self, o):
      if isinstance(o, ObjectId):
          return str(o)
      if isinstance(o, datetime.datetime):
          return str(o)
      return json.JSONEncoder.default(self, o)

def toJson(data):
  """Convert Mongo object(s) to JSON"""
  return json.dumps(data, default=json_util.default)
        
################
#### routes ####
################
 
 
@messages_blueprint.route('/messages', methods=['GET'])
def messages():
    messages = mongoDb.db.messages.find()
    json_results = []
    for message in messages:
      json_results.append(message)  
    return toJson(json_results), 200

@messages_blueprint.route('/messages', methods=['POST'])
def add_message():
    message_request = request.get_json()
    result  = mongoDb.db.messages.insert_one({
        "thema": message_request["thema"],
        "content": message_request["content"],
        "email": message_request["email"],
    }) 
    response  = mongoDb.db.messages.find_one(result.inserted_id)    
    return toJson(response), 200
    
@messages_blueprint.route('/messages/<message_id>', methods=['GET'])
def find_message(message_id):
    response  = mongoDb.db.messages.find_one({'_id': ObjectId(message_id)})    
    return toJson(response), 200

@messages_blueprint.route('/messages/thema/<thema>', methods=['GET'])
def find_message_by_thema(thema):
    messages = mongoDb.db.messages.find({"thema": thema})
    json_results = []
    for message in messages:
      json_results.append(message)  
    return toJson(json_results), 200

@messages_blueprint.route('/messages/<message_id>', methods=['DELETE'])
def delete_message(message_id):
    response  = mongoDb.db.messages.delete_one({'_id': ObjectId(message_id)})    
    return toJson(response.raw_result), 200

@messages_blueprint.route('/messages/thema/<thema>', methods=['DELETE'])
def delete_thema(thema):
    response  = mongoDb.db.messages.delete_many({'thema': thema})    
    return toJson(response.raw_result), 200
    