import sys
import os

from project import db
from project.models import Recipe, User
 
 
# create the database and the database table
db.create_all()


# Insert user data
user1 = User(email='patkennedy79@yahoo.com', plaintext_password='password1234', role='user')
user2 = User(email='kennedyfamilyrecipes@gmail.com', plaintext_password='PaSsWoRd', role='user')
user3 = User(email='blaa@blaa.com', plaintext_password='MyFavPassword', role='user')
db.session.add(user1)
db.session.add(user2)
db.session.add(user3)

# Commit the changes for the users
db.session.commit()

# Insert recipe data
recipe1 = Recipe('Slow-Cooker Tacos', 'Delicious ground beef that has been simmering in taco seasoning and sauce.  Perfect with hard-shelled tortillas!', user2.id, False)
recipe2 = Recipe('Hamburgers', 'Classic dish elevated with pretzel buns.', user2.id, True)
recipe3 = Recipe('Mediterranean Chicken', 'Grilled chicken served with pitas, hummus, and sauted vegetables.', user2.id, True)

db.session.add(recipe1)
db.session.add(recipe2)
db.session.add(recipe3)

# commit the changes for the recipes
db.session.commit()